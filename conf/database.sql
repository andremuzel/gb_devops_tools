/* Cria a tabela de usuários */

use gb_devops_tools;

CREATE TABLE users (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  username varchar(255) DEFAULT NULL,
  status int(11) DEFAULT NULL,
  token varchar(255) DEFAULT NULL,
  login_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY id_UNIQUE (id),
  UNIQUE KEY token_UNIQUE (token)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

/* Metadata */
CREATE TABLE metadata (
  mkey varchar(255) DEFAULT NULL,
  mvalue text DEFAULT NULL,
  UNIQUE KEY mkey_UNIQUE (mkey)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
