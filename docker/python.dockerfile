FROM python

#RUN mkdir /app ; \
#    git clone https://gitlab.com/andremuzel/gb_devops_tools ; \
#    cp -rf gb_devops_tools/app/ / ; \
RUN    pip install flask flask-cors mysql Flask-Caching ldap3 mysql-connector-python jenkinsapi ; 

#RUN echo "show databases" | mysql -u root --password=password -h gb_tools_db

CMD export FLASK_APP=/app/breeze.py ; \
    export FLASK_ENV=development ; \
    flask run --host 0.0.0.0
