# Growth Businness DevOps Tools
> Uma interface que auxilia na operação diária da área de GB

## Instalação 


Clonar o repositório:
```
git clone https://gitlab.com/andremuzel/gb_devops_tools.git 
```

Rodar o docker-compose:
```
docker-compose --env-file .env up -d
```
