URL_APP="app"


// send l0g1n data
function login() {
  user_name = $('[frm-user-name]').val()
  user_pass = $('[frm-user-pass]').val()

  d = ""

  $.ajax({
    url: URL_APP + "/login",
    method: "POST",
    
    data : {
      'user_name' : user_name,
      'user_pass' : user_pass
    },
    success: function(data) {
      d = data
      check_login(data)

    },
    complete:function() {
      if(d['status'] == 403) {
        msg = d['message']
        $('.login-message').html("<div class='alert alert-danger' role='alert'>"+msg+"</div>")
      } else if(d['status'] == 302) {
        $('.login-message').hide()
      }
    }
  })
}

// Valida l0g1n data
var token = localStorage.getItem('token')
function check_login(data) {
  
  status = data['status']
  
  if(status == 302) {
    user_token = data['user_token']
    localStorage.setItem('token', user_token)
    token = localStorage.getItem('token')

    
    start_webapp();
    
  } else {
    
    // Sem login, sem app   
    $("#login_app").css({
      'display' : 'flex'
    });

  }

}

// Inicia o site
function start_webapp() {
  
  // Contrução do site e start da verificaçãode login
  $("#login_app").hide()

  $.ajax({
    url: URL_APP + "/check/login?token=" + token,
    success: function(data) {
      user_name = data['user_name']
      $("[label-username").html(user_name)
    },
    complete: function(){
      
      // Show web app
      $("#web_app").show();
      
      // Load menu
      try {

        $.ajax({
          url: "modules/modules.json",

          success: function(data) {
            $("#main-menu > .menu > ul").empty()
            for( d in data) {
              $("#main-menu > .menu > ul").append(`<li><a mod_dir='${data[d]['dir_name']}'><i class='${data[d]['icon']}'></i>${data[d]['name']}</a></li>`)
            }
          },
          error: function(a,b) {
            document.write("Erro ao carregar o menu!")    
          }
        });

      } catch {

        document.write("Erro ao carregar o menu")

      }
      
      // Start loop
  var loop_interval = setInterval(function(){ 
    $.ajax({
      url: URL_APP + "/check/login?token=" + token,
      success: function(data) {
        
        status = data['status']
        if(status == 403) {
          clearInterval(loop_interval)
          $("#main-menu > .menu > ul").empty()
          $("#web_app").hide()
          $("#web_app > #page").empty()
          // Sem login, sem app   
          $("#login_app").css({
            'display' : 'flex'
          });
        }


      },
      error: function(a,b) {
        if(b == 'error') {
          document.write("Application error!")
        }
      }
    })
  }, 10000)


    }
  })

}

// logofff
function logoff() {
  $.ajax({
    url: URL_APP + "/logoff?token=" + token,
    success: function(data) {
      window.location = ""
    }
  })
}


$(document).ready(function() {
  $.ajax({
    url: URL_APP + "/check/login?token=" + token,
    success: function(data) {
      check_login(data)
    },
    error: function(a,b) {
      if(b == 'error') {
        document.write("Application error!")
      }
    }
  }) 
  
  

})

// Menu submenu action
active_module = ""
$("#web_app").find(".menu").on("click","ul > li > a", function() {
  $( this ).closest(".menu").find("a").removeClass("active")
  $( this ).addClass("active")
  mod_dir = $( this ).attr("mod_dir")
  active_module = mod_dir
  mod_page = 'modules/' + mod_dir + '/html/index.html'
  $("#page").empty()
  $("#page").load(mod_page,function() {

    // Get active button and load page
    p  = $("#page").find(".submenu > ul > li > .active").attr("page")
    active_page = 'modules/' + mod_dir + '/html/' + p
    $("#page").find("#load > #content").load(active_page)
    

  })
  
});

$("#web_app").on("click",".submenu > ul > li > a", function() {
  $( this ).closest(".submenu").find("a").removeClass("active")
  $( this ).addClass("active")

  page = $( this ).attr('page')
  mod_subpage = 'modules/' + active_module + '/html/' + page
  $("#page").find("#load > #content").empty()
  $("#page").find("#load > #content").load(mod_subpage)
})

// Page error
function page_error(p) {
  document.write(p)
}

// form validator
function check_form(f) {
  
  validated = true
  $("#page").find("#load").find("[name='form-set-jenkins-server']").find("input[required]").each(function() {

    val = $(  this ).val();

    if( val == "" ) {
      $( this ).addClass("is-invalid")
      validated = false
    } else {
      $( this ).removeClass("is-invalid")
    }

  });

  return validated

}


