PYTHON_API_SERVER='http://192.168.0.111:5000'

function consultar_minion_id(e) {

    // Verifica se o input está vazio ou se o Salt Master nao foi escolhido
    input_minion_id = $("[name='minion_id']").val()
    salt_master_env = $("#select_salt_env").val()
    
    if(salt_master_env == 0) {

        // Adiciona classe is-invalid
        $("#select_salt_env").addClass("is-invalid")
        return

    } else {

        $("#select_salt_env").removeClass("is-invalid")

        if(input_minion_id == "") {
            
            // Adiciona classe is-invalid
            $("[name='minion_id']").addClass("is-invalid")
            return

        } else {
            
            $("[name='minion_id']").removeClass("is-invalid")
            
            target = e.target

            // Inicia a consulta
            
            $.ajax({
                url: PYTHON_API_SERVER + '/api/servidores/aceite/checklist?minion_id=' + input_minion_id + '&master_env=' + salt_master_env,
                dataType: "JSON",
                beforeSend: function() {

                    // Ativa o loading no botao
                    
                    $( target ).prop("disabled","disabled")
                    $( target ).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="margin-right:15px"></span> Consultando...')

                    // Desabilita input
                    $("[name='minion_id']").prop("disabled","disabled")

                },
                success: function(data) {
                    

                    consultar_minion_id_render(data)

                },
                error: function(a,b) {

                },
                complete: function() {

                    // Ativa o loading no botao
                    $( target ).removeAttr("disabled")
                    $( target ).html('Consultar')

                    // Habilita input
                    $("[name='minion_id']").removeAttr("disabled","disabled")

                }
            })

        }
    }
}

// 
function consultar_minion_id_render(d) {
    
    m_container = ""

    for(minion in d) {
        
        if(d[minion]['success'] == true) {
            
            count_ok = 0
            count_nok = 0;
            count_all = 0
            item_list = ""
            for(item in d[minion]['data']) {
                
                status = d[minion]['data'][item]
                if(status == 'False') {
                    item_list = item_list.concat('<div class="checklist-item item-nok"><i class="fal fa-times"></i><span>' + item + '</span></div>')
                    count_nok++;
                } else if(status == 'True') {
                    item_list = item_list.concat('<div class="checklist-item item-ok"><i class="fal fa-check"></i><span>' + item + '</span></div>')
                    count_ok++;                    
                }
                
                count_all++

            }

            if(count_ok == count_all) {
                icon_check = '<i class="fas fa-check-circle"></i>'
            } else if(count_nok == count_all) {
                icon_check  = '<i class="fas fa-times-circle">'
            } else {
                icon_check  = '<i class="fas fa-exclamation-circle">'
            }

            

        } else {
            icon_check  = '<i class="fas fa-times-circle">'
            item_list = d[minion]['message']
        }

        

        m_container = m_container.concat('<div class="container-fluid"  >')
        m_container = m_container.concat('<div class="row" style="margin-bottom: 40px;">');
        m_container = m_container.concat('<div class="col-sm">');
        m_container = m_container.concat('<div class="container-aceite-servidor" style="background:#333" >');
        m_container = m_container.concat('<table width="100%" style="cursor:pointer" onclick="show_minion_checklist(event)">');
        m_container = m_container.concat('<tr>');
        m_container = m_container.concat('<td status-icon valign="center"  style="width:10px">'+icon_check+'</i></td>');
        m_container = m_container.concat('<td label-minion-id valign="center">'+minion+'</td>');
        m_container = m_container.concat('</tr>');
        m_container = m_container.concat('</table>');

        m_container = m_container.concat('<div style="margin:10px 0px;display:none" id="minion_checklist">'+item_list+'</div>');

        m_container = m_container.concat('</div>');
        m_container = m_container.concat('</div>');
        m_container = m_container.concat('</div>');
        m_container = m_container.concat('</div>');

    }

    $("#aceite_checklist_result").html(m_container)

}

function show_minion_checklist(e) {
    
    target = e.target
    
    d = $( target ).closest('.container-fluid').find("#minion_checklist")
    is_visible = $( d ).is(":visible")
    if(is_visible) {
        $( d ).hide()
    } else {
        $( d ).show()
    }

    
    

}