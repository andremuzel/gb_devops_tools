#!/usr/bin/python3
#
# Esse script tem como objetivo criar a estrutura de 
# diretórios e arquivos para a criação de um modulo para o GB_TOOLS

import sys,os,pathlib,json



def makeModule(p):
    
    # Create dir
    dir_name = '{}/{}'.format(pathlib.Path(__file__).parent,p['dir_name'])
    try:
        os.mkdir(dir_name)
        os.mkdir(dir_name + "/html")
        os.mkdir(dir_name + "/css")
        os.mkdir(dir_name + "/js")
        os.mkdir(dir_name + "/img")

        # Create empty files
        try:
            f1 = open(dir_name + "/js/index.js", 'w')
            f1.close()
            f2 = open(dir_name + "/css/index.css", 'w')
            f2.close()
            f3 = open(dir_name + "/html/index.html", 'w')
            html_string = '<!-- Main JS -->'
            html_string = html_string + '\n<script src="modules/{}/js/index.js"></script>'.format(dir_name)
            html_string = html_string + '\n\n<!-- Main CSS-->'
            html_string = html_string + '\n<link href="modules/{}/css/index.css" rel="stylesheet">'.format(dir_name)

            html_string = html_string + '\n\n<!-- Submenu -->'
            html_string = html_string + '\n<div class="submenu">'
            html_string = html_string + '\n     <ul>'
            html_string = html_string + '\n         <li><a page="page_1.html" class="active">pagina 1</a></li>'
            html_string = html_string + '\n         <li><a page="page_2.html" >pagina 2</a></li>'
            html_string = html_string + '\n         <li><a page="page_3.html">pagina 3</a></li>'
            html_string = html_string + '\n         <li><a page="page_4.html">pagina 4</a></li>'
            html_string = html_string + '\n     </ul>'
            html_string = html_string + '\n</div>'

            html_string = html_string + '\n\n<!-- Load Page content-->'
            html_string = html_string + '\n<div id="load">'
            html_string = html_string + '\n    <div id="content">'
            html_string = html_string + '\n    </div>'
            html_string = html_string + '\n</div>'
            f3.write(html_string)
            f3.close()
            f4 = open(dir_name + "/html/page_1.html", 'w')
            f4.write('page 1')
            f4.close()
            f5 = open(dir_name + "/html/page_2.html", 'w')
            f5.write('page 2')
            f5.close()
            f6 = open(dir_name + "/html/page_3.html", 'w')
            f6.write('page 3')
            f6.close()
            f7 = open(dir_name + "/html/page_4.html", 'w')
            f7.write('page 4')
            f7.close()

            # Create module.json
            mod_list = {
                "dir_name" : p['dir_name'],
                "name" : name,
                "description" : description,
                "icon" : "fas fa-skull-crossbones"
                
            }
            f8 = open(dir_name + "/module.json", 'w')
            f8.write(json.dumps(mod_list))
            f8.close()
            
        except:
            print("erro to create files")

    except:
        print('erro to create dir')






args = sys.argv
if '--dir-name' in args and '--name' in args and '--description' in args:
    
    for i,a in enumerate(args):
    
        if args[i] == "--dir-name":
            dir_name = args[i+1]
    
        if args[i] == "--name":
            name = args[i+1]       
    
        if args[i] == "--description":
            description = args[i+1]

    makeModule({
        "dir_name" : dir_name,
        "name" : name,
        "description" : description
    })

elif '--update-modules' in args:
    # Create modules.json in main modules dir
    module_list = []
    for d in os.listdir(pathlib.Path(__file__).parent):
        dir = '{}/{}'.format(pathlib.Path(__file__).parent,d)
        if os.path.isdir(dir):
            json_file = open('{}/module.json'.format(dir),'r')
            json_data = json.load(json_file)
            module_list.append(json_data)

    mf = open('{}/modules.json'.format(pathlib.Path(__file__).parent),'w')        
    mf_data = str(json.dumps(module_list))
    mf.write(mf_data)
    mf.close()

else:
    p_dir_name = "nome_do_diretorio"
    p_mod_name = "Nome do modulo"
    p_description = "Descrição/detalhes/inormação do modulo"
    print('Ex.:\n\t./createModule.py --dir-name {} --name "{}"  --description "{}"'.format(p_dir_name,p_mod_name,p_description))
    print('\t./createModule.py --update-modules : Atualiza o arquivo json responsável pelo menu\n')
    sys.exit()





