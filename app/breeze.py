#!/usr/bin/env python3
#
#
#

import pathlib
import sys
import os

from flask import Flask, request, jsonify
from flask_cors import CORS

# Adiciona diretório lib no path do Python
lib_to_add = "{}/{}".format(os.path.dirname(os.path.abspath(__file__)), "lib")
sys.path.insert(0, lib_to_add)

from lib.login import Login

# Flask APP
app = Flask(__name__)

# CORS
CORS(app)

# Teste
@app.route('/teste')
def teste():
    pass
    
# LOGIN
@app.route('/app/login',methods = ['POST','GET'])
def login():
  
    if request.method == "POST":
        param = {
            'user_name' : request.form['user_name'],
            'user_pass' :request.form['user_pass']
        }
        return Login(param).login()
    if request.method == "GET":
        return jsonify({
            'status': 0,
            'message': 'Access denied!'
        })

# Valida login
@app.route('/app/check/login')
def check_login():
    return Login('').get_token()

# logoff
@app.route('/app/logoff')
def logoff():
    return Login('').logoff() 
