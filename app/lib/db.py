import mysql.connector
import configparser
import pathlib
import os
from mysql.connector.errors import Error


class Db:

    def __init__(self):
        #self.query = query
        pass

    def select(self, query_string):

        #db_config = configparser.ConfigParser()
        #db_config.read(
        #    '{}/db_config.ini'.format(pathlib.Path(__file__).parent))

        MYSQL_ROOT_PASSWORD=os.getenv('MYSQL_ROOT_PASSWORD')
        MYSQL_DATABASE=os.getenv('MYSQL_DATABASE')
        MYSQL_HOSTNAME=os.getenv('MYSQL_HOSTNAME')

        conn = mysql.connector.connect(
            user="root",
            password=MYSQL_ROOT_PASSWORD,
            database=MYSQL_DATABASE,
            host=MYSQL_HOSTNAME
        )

        mycursor = conn.cursor()
        mycursor.execute(query_string)
        res = mycursor.fetchall()
        conn.commit()
        mycursor.close()
        conn.close()
        return res

    # insert/update
    def insert_update(self, query_string):

        MYSQL_ROOT_PASSWORD=os.getenv('MYSQL_ROOT_PASSWORD')
        MYSQL_DATABASE=os.getenv('MYSQL_DATABASE')
        MYSQL_HOSTNAME=os.getenv('MYSQL_HOSTNAME')

        conn = mysql.connector.connect(
            user="root",
            password=MYSQL_ROOT_PASSWORD,
            database=MYSQL_DATABASE,
            host=MYSQL_HOSTNAME
        )

        mycursor = conn.cursor()
        mycursor.execute(query_string)
        conn.commit()
        mycursor.close()
        conn.close()
        return True
