import mysql.connector
import json
import time
import hashlib
import os
from mysql.connector.errors import Error
from ldap3 import Server, Connection, ALL, SUBTREE
from ldap3.core.exceptions import LDAPException, LDAPBindError
from flask import jsonify, request
from db import Db




class Login:

    def __init__(self, param):
        self.param = param

    def login(self):
        
        p = self.param
        user_name = p['user_name']
        user_pass = p['user_pass']

        try:

            ldap_server = os.getenv('LDAP_SERVER')
            ldap_server_port = os.getenv('LDAP_SERVER_PORT')

            # OPENLDAP DEV
            ## server_uri = "ldap://{}:{}".format(ldap_server,ldap_server_port)
            ## server = Server(server_uri, get_info=ALL)
            ## connection = Connection(server,
            ##                         user='cn='+user_name+',dc=example,dc=org',
            ##                         password=user_pass)
            # OPENLDADP PROD
            server = Server(ldap_server, port =int(ldap_server_port), use_ssl = True)  # define a secure LDAP server
            connection = Connection(server,user=user_name,password=user_pass)


            bind_response = connection.bind()  # Returns True or False
            
            if bind_response:
                
                # Cria token
                user_string_hash = user_name + "%.20f" % time.time()
                token = hashlib.md5(
                    user_string_hash.encode('utf-8')).hexdigest()

                # Verifica se o usuário existe na base de dados
                query = 'select count(*) from users where username="{}"'.format(user_name)
                
                res = Db().select(query)
                
                user_auth = 0  # flag para mostrar que o usuário está ou não authenticado

                # Verifica se o usuário existe ou não
                result = res[0][0]
                
                if result == 0:  # não existe
                    
                    # Cria entrada na  tabela user e adiciona um token de acesso
                    query_add_user = 'insert into users  (username,status,token,login_date) values ("{}",{},"{}",NOW())'.format(
                        user_name, 1, token)
                    
                    
                    
                    try:
                        Db().insert_update(query_add_user)    
                        user_auth = 1
                        
                    except Exception as e:
                        return str(e)
                    
                elif result == 1:  # existe
                    
                    # Atualiza o token de acesso
                    # Cria entrada na  tabela user e adiciona um token de acesso
                    query_update_user = 'update users set token="{}", login_date=NOW() where username="{}"'.format(
                        token, user_name)
                    
                    
                    try:
                        
                        Db().insert_update(query_update_user)
                        user_auth = 1
                        
                        
                    except Exception as e:
                        return jsonify({
                            'status': 0,
                            'message': str(e)
                        })

                
                if user_auth == 1:
                    # Pega as informações do banco e envia para o solicitante
                    query_user_info = 'select * from users where username="{}" and token="{}"'.format(
                        user_name, token)
                    
                    user_info_query = Db().select(query_user_info)
                    
                    user_info_res = user_info_query[0]

                    data = {
                        'user_id': user_info_res[0],
                        'user_name': user_info_res[1],
                        'user_status': user_info_res[2],
                        'user_token': user_info_res[3],
                        'status' : 1
                    }
                    return jsonify(data)
            # Login error
            else:

                return jsonify({
                    'status': 0,
                    'message': 'Access denied!'
                })

        except Exception as e:
            return jsonify({
                    'status': 0,
                    'message': str(e)
            })


    # logoff
    def logoff(self):
        token = request.args['token']
        query = 'update users set token="" where token="{}"'.format(token)

        Db().insert_update(query)      
        return ''


    # Valida token
    def get_token(self):
        
        # Se parametro 'token' existe
        if 'token' in request.args.keys():
                
            token = request.args['token']    
            query = "select * from users where token='{}'".format(token)
            q = Db().select(query)
            
            res_len = len(q)
            
            result = res_len
               
            
            

            if result == 0:
                return jsonify({
                    'status': 0,
                    'message': 'Access denied!'
                })
            else:
                query_result = q[0]
                user_data = {}
                user_data = {
                    'user_id': query_result[0],
                    'user_name': query_result[1],
                    'user_status': query_result[2],
                    'user_token': query_result[3],
                    'status' : 1
                }
                return jsonify(user_data)
        # Se parametro token nao existe
        else:
            return jsonify({
                    'status': 0,
                    'message': 'Access denied!'
                })    
        

   
