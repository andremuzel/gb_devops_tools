export FLASK_APP=breeze.py 
export FLASK_ENV=development

export LDAP_SERVER=192.168.0.111
export LDAP_SERVER_PORT=389
export MYSQL_DATABASE="gb_tools"
export MYSQL_HOSTNAME=192.168.0.111
export MYSQL_ROOT_PASSWORD="passw0rd"



echo -n "\n---> Start breeze.py at: $(date)" >>  logs/breeze.log
echo -n "\n================================" >>  logs/breeze.log
flask run --host 0.0.0.0 >> logs/breeze.log 2>> logs/breeze.log &
